all: test doc build clean

test:
	python tests/area_test.py
	python tests/component_test.py

doc:
	cd docs && $(MAKE) html

clean:
	find . -name '*.pdf' -delete
	find . -name '*.gv' -delete
	find . -name '*.pyc' -delete
	rm -r GraphGenerator.egg-info

build:
	python setup.py sdist

example:
	python3 example.py
