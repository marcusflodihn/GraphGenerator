import json
from graphviz import Digraph

class Graph(object):

	def __init__(self):
		self.areas = list()
		self.components = list()
		self.connections = list()
		self.entities = list()
		self.digraph_object = Digraph('G')

	def add_area(self, area):
		''' Adds an area to the graph

		Args:
			area(graph_generator.Area)

		Returns:
			None
		'''
		self.areas.append(area)

	def add_component(self, component):
		''' Adds a component to the graph

		Args:
			graph_generator.Component

		Returns:
			None
		'''
		self.components.append(component)

	def add_components(self, components):
		for component in components:
			self.components.append(component)

	def add_connection(self, connection):
		''' Adds an connection to the graph

		Args:
			graph_generator.Connection

		Returns:
			None
		'''
		self.connections.append(connection)

	def parse(self, output):
		''' Parses all the areas that are
		contained in the graph.

		Args:
			output(file output)

		Returns:
			None
		'''
		with open(output, 'w') as output_file:
			for area in self.areas:
				area.parse_components(output_file)
			for component in self.components:
				component.parse_self(output_file)

	def render(self):
		''' Function for rendering the entire graph
		to an PDF-file using Graphviz.

		Args:
			None

		Return:
			None
		'''
		self.digraph_object.attr(compound='true')
		for index, area in enumerate(self.areas, 1):
			cluster_name = 'cluster' + str(index) + area.name
			with self.digraph_object.subgraph(name=cluster_name) as subgraph:
				subgraph.attr(label=area.name)
				for component in area.components:
					subgraph.node(component.name)
					for connection in component.connections:
						self.digraph_object.edge(
							component.name,
							connection['target'].name,
							label=connection['connection_type'],
							_attributes={'arrowhead': 'none'}
						)
		for component in self.components:
			self.digraph_object.node(component.name)
			for connection in component.connections:
				self.digraph_object.edge(
								component.name,
								connection['target'].name,
								label=connection['connection_type'],
								_attributes={'arrowhead': 'none'}
							)
		self.digraph_object.view()
