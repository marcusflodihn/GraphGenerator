class Area(object):

	def __init__(self, name):
		self.name = name
		self.components = list()
		self.connections = list()

	def add_component(self, component):
		self.components.append(component)

	def add_components(self, components):
		for component in components:
			self.components.append(component)

	def parse_components(self, output):
		for component in self.components:
			component.parse_self(output)

	def __str__(self):
		return self.name
