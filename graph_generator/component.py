class Component(object):

	def __init__(self, name, component_type=None):
		self.name = name
		self.is_connected = False
		self.sub_components = list()
		self.connections = list()
		if component_type is not None:
			self.component_type = component_type


	def add_sub_component(self, sub_component):
		''' Adds a sub-component to the component.
		'''
		self.sub_components.append(sub_component)


	def connect(self, connection_type, target):
		''' Connects the component with another component,
		and adds a type for the connection.
		'''
		self.connections.append({
			'connection_type': connection_type,
			'target': target
			})
		target.is_connected = True


	def parse_self(self, output):
		''' Parses the component, its connections and
		its sub-components.
		'''
		self.define_component(output)
		if len(self.connections) > 0:
			self.define_connections(output)
		if len(self.sub_components) > 0:
			self.define_sub_components(output)
		if len(self.connections) > 0 or len(self.sub_components) > 0:
			output.write('end\n\n')


	def define_connections(self, output):
		''' Define all the connections for the
		object.
		'''
		connection_string = ''
		unique_connection_types = self.get_unique_connection_types()
		for connection_type in unique_connection_types:
			output.write('\t{}\n'.format(connection_type))
			for index, connection in enumerate(self.get_connections_with_type(connection_type), 1):
				if connection_type == 'dataconnection':
					connection_string = 'dc'
				if connection_type == 'powerconnection':
					connection_string = 'pc'
				if index != len(self.connections):
					output.write('\t\t{}{}: {};\n'.format(connection_string, index, connection['target']))
				else:
					output.write('\t\t{}{}: {}\n'.format(connection_string, index, connection['target']))


	def define_sub_components(self, output):
		''' Define all the sub-components for the
		object.
		'''
		if len(self.sub_components) > 0:
			output.write('\tsubcomponent\n')
			for index, sub_component in enumerate(self.sub_components, 1):
				connection_name = 'c' + str(index)
				output.write('\t\t{}:{}\n'.format(connection_name, sub_component.name))


	def define_component(self, output):
		''' Defines the name of the object,
		and the type of the object. Also defines
		which connection the object has to other
		objects.
		'''
		if len(self.connections) > 0:
			output.write('{} in {} with\n'.format(self.name, self.component_type))
		else:
			output.write('{} in {}\nend\n\n'.format(self.name, self.component_type))


	def get_unique_connection_types(self):
		''' Finds all the unique connection types
		in all the different connection that the
		component has.
		'''
		result = list()
		for connection in self.connections:
			if connection['connection_type'] not in result:
				result.append(connection['connection_type'])
		return result


	def get_connections_with_type(self, connection_type):
		''' Looks through all the connection in a object
		and returns the connections that has a certain
		connection type.
		'''
		result = list()
		for connection in self.connections:
			if connection['connection_type'] == connection_type:
				result.append(connection)
		return result


	def __str__(self):
		return self.name
