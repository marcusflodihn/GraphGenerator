class Connection(object):

	def __init__(self, head, tail, bidirectional, graph):
		self.head = head
		self.tail = tail
		self.bidirectional = bidirectional
		graph.connections.append(self)

