class Component(object):
    """ An object representing a single component
    that can be a part of either the Graph or Area
    object.
    """

    def __init__(self, name, component_type, parent=None):
        #self.connections = list()
        self.subcomponents = list()
        self.attributes = dict()
        self.name = name
        self.component_type = component_type
        if parent is not None:
            parent.add_component(self)


    def add_subcomponent(self, subcomponent):
        self.subcomponents.append(subcomponent)

    #def connect(self, component, bidirectional, connection_type):
        """ Function for creating a connection between
        two different Component objects.

        Change this to a seperate function / object named "Connection",
        the start of the connection is the component from which it is 
        called from, which makes this slightly unclear.

        Args:
            component (Component): The target component that will
            be connected with the calling object.
            bidirectional (Boolean): Specifies if the connection
            is bidirectional, if not the connection goes from head
            (calling object) to tail(component argument).
            connection_type (String): A string representing the
            type of the connection.
        """

        #connection = {
        #    'head': self.name,
        #    'tail': component.name,
        #    'bidirectional': bidirectional,
        #    'connection_type': connection_type
        #}
        #self.connections.append(connection)

    def add_attribute(self, attribute, value):
        self.attributes[attribute] = value

    def __str__(self):
        return self.name
