import graphviz
from .parsers import export_otelos


class Graph(object):
    """ The graph object represent the "root" of the graph.
    """

    areas = list()
    digraph_object = graphviz.Digraph('G', filename='test')

    def __init__(self):
        self.components = list()
        self.connections = list()

    def resolve(self):
        self.resolve_areas()
        self.resolve_self()

    def resolve_areas(self):
        for area in self.areas:
            with self.digraph_object.subgraph(
                name=area.name
            ) as subgraph:
                subgraph.attr(label=area.name)
                self.create_area_components(area, subgraph)
                self.connect_area_components(area, subgraph)

    def resolve_self(self):
        """ Function for creating and connecting
        all the components that exists in the root
        graph.

        Returns:
            None
        """
        for component in self.components:
            self.digraph_object.node(component.name)
        for connection in self.connection:
            if connection.bidirectional is True:
                self.digraph_object.edge(connection.head.name,
                                            connection.tail.name,
                                            label=connection['connection_type'],
                                            _attributes={'dir': 'both'}
                )
            else:
                self.digraph_object.edge(connection.head.name,
                                            connection.tail.name,
                                            label=connection['connection_type']
                )

    @staticmethod
    def create_area_components(area, subgraph):
        """ Creates all the components of a
        certain Area object.

        Args:
            area (Area): The Area object where
            the components that should be created
            exists.
            subgraph (Digraph.subgraph): The subgraph
            object that exists in a Area object.

        Returns:
            None
        """
        for component in area.components:
            subgraph.node(component.name)

    def connect_area_components(self, area, subgraph):
        """ Connects all the components in a
        certain Area object.

        Args:
            area (Area): The Area object where
            the components that should be connected
            exists.
            subgraph (Digraph.subgraph): The subgraph
            object that exists in a Area object.

        Returns:
            None
        """
        for component in area.components:
            if self.is_component_standalone(component) is True:
                for connection in component.connections:
                    if connection['bidirectional'] is True:
                        subgraph.edge(connection['head'],
                                      connection['tail'],
                                      label=connection['connection_type'],
                                      _attributes={'dir': 'both'})
                    else:
                        subgraph.edge(connection['head'],
                                      connection['tail'],
                                      label=connection['connection_type'])

    def is_component_standalone(self, target_component):
        for area in self.areas:
            for component in area.components:
                if component.name == target_component.name:
                    return True
        return False

    def add_component(self, component):
        """ Adds a component to the root graph
        object.

        Args:
            component (Component): The component
            object to add.

        Returns:
            None
        """
        self.components.append(component)

    def export(self, file_path, export_format):
        """ Exports the graph to a specified
        format.

        Args:
            export_format (String): Output format
            file_path (String): Output location

        Return:
            result (String): The file path of
            the export.
        """
        if export_format == 'o-telos':
            export_otelos(self, file_path)

    def view(self):
        self.digraph_object.view()
