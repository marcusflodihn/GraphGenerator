class Area(object):
    """ The area object represents an environment
    that contains one or more individual component.
    """

    node_distance = 1

    def __init__(self, name, area_type=None, parent=None, node_distance=None):
        self.components = list()
        """Contains a list of Component objects that
        belongs to this area.
        """
        self.name = self.generate_name(name)
        self.type = type
        if parent is not None:
            self.parent = parent
            parent.areas.append(self)
        if node_distance is not None:
            self.node_distance = node_distance

    @staticmethod
    def generate_name(name):
        """ In order for Graphviz to see an area
        as a sub-graph it needs to have the prefix:
        'cluster'.

        Args:
            name (String): The name of the graph.

        Returns:
            The name of the area prefixed with 'cluster'

        Raises:
            ValueError: The argument has to be a string
        """
        if type(name) is not str:
            raise ValueError('The name was not a string')
        return 'cluster_{}'.format(name)

    def add_component(self, component):
        """ Adds a single component object to the Area
        object.

        Args:
            component (Component): The component that will
            be added to the Area object.

        Returns:
            None
        """
        self.components.append(component)

    def add_components(self, components):
        """ Adds a several component objects to the Area
        object.

        Args:
            components (list(Components)): A list with components
            that will be added to the Area object.

        Returns:
            None
        """

        for component in components:
            self.components.append(component)

    def __str__(self):
        return self.name
