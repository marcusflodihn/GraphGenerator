def export_otelos(root_node, file_output):
    """ Function for parsing a graph object
    to a O-Telos formatted file.

    Args:
        root_node (graph_generator.Graph): The Graph object
        which to parse.
        file_output (String): The filepath to the output file.
    """

    with open(file_output, 'w+') as output:
        define_unique_otelos_classes(root_node, output)
        for area in root_node.areas:
            write_otelos_area(output, area)


def write_otelos_area(file, area):
    for component in area.components:
        file.write('{} in {} \nend\n\n'.format(
            component.name,
            area.name)
        )
        #write_otelos_component(file, component)

def find_otelos_classes(graph):
    """Function that loops over all the unique classes
    (components) in the graph and saves them to a dictionary.
    :param graph: The graph to find unique classes in.
    :param file: File to write to.
    :return: Dictionary with all the unique classes.
    """
    result = dict()
    for area in graph.areas:
        for component in area.components:
            if component.component_type not in result:
                result[component.component_type] = component
    return result

def define_unique_otelos_classes(graph, output):
    """ Defines all the unique classes once. This
    includes the class itself and its attributes.
    :param graph: The graph where to search for unique
    classes.
    :param output: The file object to write to.
    :return: None
    """
    components = find_otelos_classes(graph)
    for key, value in components.items():
        output.write('{} with \n'.format(key))
        output.write('\t attribute\n')
        for attribute in components[key].attributes:
            output.write('\t\t{}\n'.format(attribute))
        output.write('\n')

def write_otelos_component(file, component):
    file.write('{} with \n'.format(component.name))
    for connection in component.connections:
        file.write('\t{}\n'.format('attribute'))


# def write_otelos_classes(graph, file):
#     components = get_all_unique_classes(graph)
#     for key, value in components.items():
#         file.write('{} with \n'.format(value.component_type))
#         file.write('\t attribute \n')
#         for attribute in value.attributes:
#             file.write('\t \t {}:'.format(attribute))
#
#
# def write_otelos_connections(graph, file):
#     components = get_all_unique_classes(graph)
#     for key, value in components.items():
#         file.write('{} with \n'.format(value.component_type))
#         file.write('\t attribute \n')
#         for attribute in value.attributes:
#             file.write('\t \t {}:'.format(attribute))
#         file.write('\n')


def write_otelos_subcomponents(graph, file):
    for area in graph.areas:
        for component in area.components:
            file.write('{} with \n \t subcomponent \n'.format(component.name))
            index = 0
            for subcomponent in component.subcomponents:
                file.write('\t \t c{}:{}'.format(index, subcomponent.name))
                index += 1
