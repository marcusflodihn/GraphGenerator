#!/usr/bin/python
import json
import random
import argparse
import graph_generator

unique_id = 0

def get_unique_id():
        global unique_id
        unique_id += 1
        return str(unique_id)


def read_json_file(filepath):
    with open(filepath, 'r') as settings_file:
        return json.load(settings_file)

def get_component_by_id(component_id, area):
    ''' Looks through all the components in a area and
    looks if the id of the component is the same as the
    component_id argument, in that case it returns the
    component.
    '''
    for component in area.components:
        if int(component.name.split('_')[1]) == component_id:
            return component


class ControlCenter(graph_generator.Area):
    def __init__(self, *args, **kwargs):
        super(ControlCenter, self).__init__(*args, **kwargs)
        router = Router(name='router_' + get_unique_id())
        firewall = Firewall(name='firewall_' + get_unique_id())
        switch = Switch(name='switch_' + get_unique_id())
        scada = ScadaServer(name='scada_' + get_unique_id())
        self.add_component(router)
        self.add_component(firewall)
        self.add_component(switch)
        self.add_component(scada)
        router.connect(
            connection_type='dataconnection', target=firewall
        )
        firewall.connect(connection_type='dataconnection', target=switch)
        switch.connect(connection_type='dataconnection', target=scada)


class ScadaServer(graph_generator.Component):
    def __init__(self, *args, **kwargs):
        super(ScadaServer, self).__init__(*args, **kwargs)
        self.component_type = 'ScadaServer'


class PowerStation(graph_generator.Area):

    def __init__(self, *args, **kwargs):
        super(PowerStation, self).__init__(*args, **kwargs)
        global unique_id
        self.color='red'
        router = Router(name='router_' + get_unique_id())
        firewall = Firewall(name='firewall_' + get_unique_id())
        switch = Switch(name='switch_' + get_unique_id())
        rtu = RemoteTerminalUnit(name='rtu_' + get_unique_id())
        mtu = MaximumTransmissionUnit(name='mtu_' + get_unique_id())
        busbar_1 = Busbar(name='busbar_' + get_unique_id())
        pmu = PowerManagementUnit(name='pmu_' + get_unique_id())
        generator = Generator(name='powergenerator_' + get_unique_id())
        transformer = Transformer(
            name='transformer_' + get_unique_id()
        )
        busbar_2 = Busbar(name='busbar_' + get_unique_id())
        self.add_component(router)
        self.add_component(switch)
        self.add_component(firewall)
        self.add_component(rtu)
        self.add_component(mtu)
        self.add_component(busbar_1)
        self.add_component(pmu)
        self.add_component(generator)
        self.add_component(transformer)
        self.add_component(busbar_2)
        router.connect(
            connection_type='dataconnection', target=firewall
        )
        firewall.connect(connection_type='dataconnection', target=switch)
        switch.connect(connection_type='dataconnection', target=mtu)
        mtu.connect(connection_type='dataconnection', target=rtu)
        mtu.connect(connection_type='dataconnection', target=pmu)
        rtu.connect(connection_type='dataconnection', target=busbar_1)
        rtu.connect(connection_type='dataconnection', target=generator)
        generator.connect(connection_type='powerconnection', target=busbar_1)
        busbar_1.connect(connection_type='powerconnection', target=transformer)
        transformer.connect(connection_type='powerconnection', target=busbar_2)

    def get_busbar(self, name):
            busbars = list()
            for component in self.components:
                    if type(component) == Busbar:
                            busbars.append(component)
            for busbar in busbars:
                    if busbar.name == name:
                            return busbar


class SubStation(graph_generator.Area):

    def __init__(self, *args, **kwargs):
        super(SubStation, self).__init__(*args, **kwargs)
        self.color='green'
        self.router = Router(name='router_' + get_unique_id())
        self.firewall = Firewall(name='firewall_' + get_unique_id())
        self.switch = Switch(name='switch_' + get_unique_id())
        self.mtu = MaximumTransmissionUnit(
            name='mtu_' + get_unique_id()
        )
        self.pmu = PowerManagementUnit(name='pmu_' + get_unique_id())
        self.rtu = RemoteTerminalUnit(name='rtu_' + get_unique_id())
        self.busbar = Busbar(name='busbar_' + get_unique_id())
        self.distrubution = PowerLine(
            name='powerline_' + get_unique_id())
        self.add_component(self.router)
        self.add_component(self.firewall)
        self.add_component(self.switch)
        self.add_component(self.mtu)
        self.add_component(self.pmu)
        self.add_component(self.rtu)
        self.add_component(self.busbar)
        self.add_component(self.distrubution)
        self.router.connect(
            connection_type='dataconnection', target=self.firewall
        )
        self.firewall.connect(
            connection_type='dataconnection', target=self.switch)
        self.switch.connect(
            connection_type='dataconnection', target=self.mtu)
        self.mtu.connect(
            connection_type='dataconnection', target=self.pmu)
        self.mtu.connect(
            connection_type='dataconnection', target=self.rtu)
        self.rtu.connect(
            connection_type='dataconnection', target=self.busbar)
        self.busbar.connect(
            connection_type='powerconnection', target=self.distrubution)

    def get_busbar(self, name):
        busbars = list()
        for component in self.components:
                if type(component) == Busbar:
                        busbars.append(component)
        for busbar in busbars:
                if busbar.name == name:
                        return busbar

class RemoteTerminalUnit(graph_generator.Component):
    def __init__(self, *args, **kwargs):
        super(RemoteTerminalUnit, self).__init__(*args, **kwargs)
        self.component_type = 'RTU'


class MaximumTransmissionUnit(graph_generator.Component):
    def __init__(self, *args, **kwargs):
        super(MaximumTransmissionUnit, self).__init__(*args, **kwargs)
        self.component_type = 'MTU'


class PowerManagementUnit(graph_generator.Component):
    def __init__(self, *args, **kwargs):
        super(PowerManagementUnit, self).__init__(*args, **kwargs)
        self.component_type = 'PMU'


class Busbar(graph_generator.Component):
    def __init__(self, *args, **kwargs):
        super(Busbar, self).__init__(*args, **kwargs)
        self.component_type = 'BusBar'


class Generator(graph_generator.Component):
    def __init__(self, *args, **kwargs):
        super(Generator, self).__init__(*args, **kwargs)
        self.component_type = 'PowerGenerator'


class PowerLine(graph_generator.Component):
    ''' A component representing an electric
    cable connecting two seperate physical
    builds, such as a powerstation and a
    substation.
    '''

    def __init__(self, *args, **kwargs):
        super(PowerLine, self).__init__(*args, **kwargs)
        self.component_type = 'PowerLine'


class Desktop(graph_generator.Component):

    def __init__(self, *args, **kwargs):
        super(Desktop, self).__init__(*args, **kwargs)
        self.component_type = 'Desktop'


class Switch(graph_generator.Component):

    def __init__(self, *args, **kwargs):
        super(Switch, self).__init__(*args, **kwargs)
        self.component_type = 'Switch'


class Router(graph_generator.Component):

    def __init__(self, *args, **kwargs):
        super(Router, self).__init__(*args, **kwargs)
        self.component_type = 'Router'


class Firewall(graph_generator.Component):
    def __init__(self, *args, **kwargs):
        super(Firewall, self).__init__(*args, **kwargs)
        self.component_type = 'Firewall'


class Transformer(graph_generator.Component):
    def __init__(self, *args, **kwargs):
            super(Transformer, self).__init__(*args, **kwargs)
            self.component_type = 'Transformer'

class WideAreaNetwork(graph_generator.Component):
    def __init__(self, *args, **kwargs):
        super(WideAreaNetwork, self).__init__(*args, **kwargs)
        self.component_type = 'wan'

def connect_firewalls_to_wan(grid, wan_nodes, settings):
        ''' Loops over all components in every area
        of the grid, if the type of the component is
        a Router, the component is connected to the
        wan node.

        Args:
                grid(graph_generator.Graph)
                wan(graph_generator.Component)

        Returns:
                None
        '''
        for area in grid.areas:
            wan_connections = list()
            if type(area) != ControlCenter:
                for component in area.components:
                        if type(component) is Router:
                            wan_connections = list()
                            if type(area) == PowerStation:
                                number_of_wan_connections = random.randint(
                                    settings['powerstations']['min_wan_connections'],
                                    settings['powerstations']['max_wan_connections']
                                )
                            if type(area) == SubStation:
                                number_of_wan_connections = random.randint(
                                    settings['substations']['min_wan_connections'],
                                    settings['substations']['max_wan_connections']
                                )
                            while len(wan_connections) < number_of_wan_connections:
                                target_wan = get_new_wan(wan_connections, grid)
                                if target_wan != None:
                                    target_wan.connect(
                                        connection_type='dataconnection',
                                        target=component
                                    )
                                wan_connections.append(target_wan)

def get_new_wan(wan_connections, graph):
    ''' Returns a wan connection an area is not
    already connected to.

    Args:
        wan_connections: List(WideAreaNetwork)
        graph: grid_generator.Graph

    Returns:
        graph_generator.Area
    '''
    for component in graph.components:
        if type(component) == WideAreaNetwork:
            if component not in wan_connections:
                return component


def get_last_busbar(station):
        ''' Returns the last busbar created in a station,
        which usually is the busbar that is connected to another
        station via an powerline component.

        Args:
                Stations: PowerStation / SubStation

        Returns:
                BusBar
        '''
        station_busbars = list()
        for component in station.components:
            if type(component) == Busbar:
                station_busbars.append(int(component.name.split('_')[1]))
        station_busbars.sort()
        return get_component_by_id(station_busbars[-1], station)


def connect_stations(powerstations, substations, settings, graph):
    index = 0
    for powerstation in powerstations:
        number_of_connections = random.randint(
            settings['powerstations']['min_substations'], # 1
            settings['powerstations']['max_substations']  # 2
        )
        busbar_to_connect = get_last_busbar(powerstation)
        for substation in substations:
            target_busbar = get_last_busbar(substation)
            if len(target_busbar.connections) < settings['substations']['max_powerstations'] + 1:
                powerline = PowerLine(name='powerline_' + get_unique_id())
                target_busbar.connect(connection_type='powerconnection', target=powerline)
                powerline.connect(connection_type='powerconnection', target=busbar_to_connect)
                graph.add_component(powerline)
                index += 1
            if index == number_of_connections:
                index = 0
                break


def generate_grid(settings, output, view='no'):
    ''' Function for generating a grid.

    Args:
            powerstation(Integer): The number of powerstations
            to be included in the grid.
            substations(Integer): The number of service-
            stations to be included in the grid.

    Returns: None
    '''
    grid = graph_generator.Graph()
    wan_list = list()
    powerstations_list = list()
    substation_list = list()
    for x in range(0, settings['wan']['total_number']):
        wan = WideAreaNetwork('wan_' + get_unique_id())
        grid.add_component(wan)
        wan_list.append(wan)
        number_of_controlstations = random.randint(
            settings['wan']['min_controlstations'],
            settings['wan']['max_controlstations']
        )
        for index in range(0, number_of_controlstations):
                controlcenter = ControlCenter(
                    name='Control Center ' + str(index)
                )
                grid.add_area(controlcenter)
                wan.connect(connection_type='dataconnection', target=controlcenter.components[0])

    for number_of_powerstations in range(0, settings['powerstations']['total_number']):
            powerstation = PowerStation(
                name='Power Station ' + str(number_of_powerstations)
            )
            grid.add_area(powerstation)
            powerstations_list.append(powerstation)
            number_of_substations = random.randint(
                settings['powerstations']['min_substations'],
                settings['powerstations']['max_substations']
            )
            for substation in range(0, number_of_substations):
                substation = SubStation(
                    name='Sub Station ' + str(substation)
                )
                grid.add_area(substation)
                powerstation_busbar = get_last_busbar(powerstation)
                substation_busbar = get_last_busbar(substation)
                powerline = PowerLine(
                    name='powerline_' + get_unique_id()
                )
                powerstation_busbar.connect(connection_type='powerconnection', target=powerline)
                powerline.connect(connection_type='powerconnection', target=substation_busbar)
                grid.add_component(powerline)

    connect_firewalls_to_wan(grid, wan_list, settings)
    connect_stations(powerstations_list, substation_list, settings, grid)
    grid.parse(output)
    if view == 'yes':
            grid.render()


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-o', type=str, metavar='--output', help='''The filepath where the
                            output o-telos file will be saved.
                            ''')
    arg_parser.add_argument('-v', type=str, metavar='--view', help='''A graph can be rendered
                            to visualize the output. To show graph enter: 'yes'.
                            ''')
    arg_parser.add_argument('-s', type=str, metavar='--settings', help=''' The filepath to the JSON-
    file containing the settings for the graph''')
    args = arg_parser.parse_args()

    generate_grid(
        settings=read_json_file(args.s),
        output=args.o,
        view=args.v
    )
