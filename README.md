# Smart Graph Generator

### Introduction

This project was started in conjunction with the [Elvira](https://www.his.se/en/Research/informatics/Distributed-Real-Time-Systems/Infrastructure-resilience/) project conducted at the University of Skövde in Sweden.

This repository contains 2 things:
- Generator script: The goal of this script is to generate a model that has a structure similar to a real powergrid, which simulations can be performed on ( this is built using the framework described below).
- Graph framework: Basically it is a 'wrapper' around the Graphviz framework, which abstracts graphs to: Component, Area and Graph objects. The framework is more semantic than pure Graphviz, and offers a object orientation.

__Official project description__

>The Elvira project develops time-based infrastructure dependency analysis for the power-grid to model risk assessment and resilience index, which assist decision makers in anticipating failures and their cascading effects.

### Requirements
* Python 3.x
* Graphviz (Python)

### Installation
Make some directory that will contain the repository

```sh
mkdir ~/graph_generator && cd ~/graph_generator
```

Clone the repository

```sh
git clone https://gitlab.com/marcusflodihn/GraphGenerator.git
```

Use pip to install the requirements for the framework

```sh
pip install -r requirements.txt
```

### Example usage

To see a description of the availible flags you can type:

```sh
python generator.py -h
```

To generate a graph with the default settings file you can type:

```sh
python generator.py -s settings.json -o ~/Desktop/output.sml.txt -v yes
```

To tweak the settings of the generation you can edit the settings.json file.

### Example

```python
import graph_generator

graph = graph_generator.Graph()

example_area = graph_generator.Area(name='Example Area')

component_a = graph_generator.Component(name="Component A")

component_b = graph_generator.Component(name="Component B")

example_area.add_components((component_a, component_b))

component_a.connect(target=component_b, connection_type='data connection')

seperate_component = graph_generator.Component(name='Seperate component')

seperate_component.connect(target=component_b, connection_type='power connection')

graph.add_component(seperate_component)

graph.add_area(example_area)

graph.render()

```
### Result
<figure>
    <img src="example.jpg" width="400"/>
    <figcaption><i>Output rendered by the code above in the example</i></figcaption>
</figure>